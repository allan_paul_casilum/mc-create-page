<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function mc_dump($array, $exit = false){
	echo '<pre>';
		print_r($array);
	echo '</pre>';
	if( $exit ){
		exit();
	}
}
function mc_get_all_post_meta_locations(){
	$list = new MC_Page;
	return $list->get_all_post_meta_locations();
}
