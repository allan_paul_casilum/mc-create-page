<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Dashboard{
	protected static $instance = null;
	public $db_prefix = 'mc_create_page_data';
	public $mc_parent_page = 'mc_parent_page';
	public $mc_canned_content = 'mc_canned_content';
	public $mc_meta_description = 'mc_meta_description';
	public $mc_locations = 'mc_meta_description';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function menu_slug(){
		return mc_get_plugin_name();
	}

	public function mc_create_page_data($action = '', $value = ''){
		$prefix = $this->db_prefix;
		switch($action){
			case 'c':
				add_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
		}
	}

	public function mc_parent_page($action = '', $value = ''){
		$prefix = $this->db_prefix;
		switch($action){
			case 'c':
				add_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
		}
	}

	public function mc_canned_content($action = '', $value = ''){
		$prefix = $this->db_prefix;
		switch($action){
			case 'c':
				add_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
		}
	}

	public function mc_meta_description($action = '', $value = ''){
		$prefix = $this->db_prefix;
		switch($action){
			case 'c':
				add_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
		}
	}

	public function mc_locations($action = '', $value = ''){
		$prefix = $this->db_prefix;
		switch($action){
			case 'c':
				add_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
		}
	}

	public function parse_data($data = ''){
		$mc_page = new MC_Page;
		return $mc_page->parse_data($data);
	}

	public function __construct(){}
}
