<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function mc_plugin_folder_name(){
	$plugin_foldername = explode('/',plugin_basename( __FILE__ ));
	return $plugin_foldername[0];
}
$mc_config = array();
$mc_config['mc_wp'] = array(
	'name' => 'mc-create-page',
	'title' => 'Create Automate Page',
	'version' => '1.0.0',
);
$mc_config['template'] = array(
	'plugin_dir_path' => plugin_dir_path( __FILE__ ),
	'public' => plugin_dir_path( __FILE__ ) . 'public/',
	'public_partials' => plugin_dir_path( __FILE__ ) . 'public/partials',
	'plugin_folder_name' => mc_plugin_folder_name(),
	'admin_dir' => plugin_dir_path( __FILE__ ) .  'admin/',
	'admin_dir_partials' => plugin_dir_path( __FILE__ ) .  'admin/partials/',
);
$mc_config['api'] = array(
	'google_public_server_key' => 'AIzaSyDUf1jAKYCcTaDkv-ZmwGT9eWYbcExyqbM',
);
function mc_get_plugin_name(){
	global $mc_config;
	return $mc_config['mc_wp']['name'];
}
function mc_get_plugin_title(){
	global $mc_config;
	return $mc_config['mc_wp']['title'];
}
function mc_get_plugin_version(){
	global $mc_config;
	return $mc_config['mc_wp']['version'];
}
function mc_admin_partials(){
	global $mc_config;
	return $mc_config['template']['admin_dir_partials'];
}
function mc_google_map_embed_api(){
	global $mc_config;
	return $mc_config['api']['google_public_server_key'];
}
function mc_public_partials(){
	global $mc_config;
	return $mc_config['template']['public_partials'];
}
	

