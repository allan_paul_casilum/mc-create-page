<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Shortcode_SuburbLocationName{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function init($atts){
		global $posts;

		$page = new MC_Page;

		$a = shortcode_atts( array(
			'location' => $page->post_meta_location_key,
		), $atts );

		$location = get_post_meta(get_the_ID(), $a['location'], true);

		return $location;
	}

	public function __construct(){
		add_shortcode( 'mc_suburb_name', array($this, 'init') );
	}
}
