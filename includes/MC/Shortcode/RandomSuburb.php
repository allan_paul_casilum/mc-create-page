<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Shortcode_RandomSuburb{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function init($atts){
		$page = new MC_Page;
		$a = shortcode_atts( array(
			'limit' => 5,
		), $atts );
		$locations = $page->get_post_meta_locations($a['limit']);

		ob_start();
		require mc_public_partials() . '/random-suburb-shortcode.php';
		$output = ob_get_clean();
		return $output;
	}

	public function __construct(){
		add_shortcode( 'mc_random_suburb', array($this, 'init') );
	}
}
