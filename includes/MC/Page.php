<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Page{
	protected static $instance = null;
	public $post_meta_location_key = 'mc_post_meta_location';
	public $post_meta_full_location_key = 'mc_post_meta_full_location';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function parse_data($data = ''){
		if(
			is_array($data)
			&& count($data) > 0
		){
			$mc_data_parse = array();
			$array_meta = array();
			//parse_data
			$parse_data = trim($data['mc_data']); // remove the last \n or whitespace character
			$parse_data = explode("\n", $parse_data);
			$post_parent = $data['mc_parent_page'];

			if(
				is_array($parse_data)
				&& count($parse_data) > 0
			){
				wp_defer_term_counting( true );
				wp_defer_comment_counting( true );
				$count_post_added = 0;
				foreach($parse_data as $key => $val){
					$post_content = '';
					$location = '';
					$meta_description = '';
					$meta_title = '';
					if(
						trim($val) != ''
					){
						//location name only
						$location_name = $val;

						//full location address
						$location = trim($location_name).' '.$data['mc_country'];

						//content
						$post_content = $data['mc_canned_content'];
						$array_string = array('%location-name%');
						$array_convert_string = array($location_name);

						//create_page
						$mc_data_parse = array(
							'post_title' => $location_name,
							'post_content' => $post_content,
							'post_parent' => $post_parent,
							'meta_description' => $data['mc_meta_description'],
						);
						$post_id = $this->create_page($mc_data_parse);
						//create_page

						//meta
						$array_string_meta_description = array('%location-name%');
						$array_convert_meta_description = array($location_name);
						$meta_description = str_replace(
							$array_string_meta_description,
							$array_convert_meta_description,
							$mc_data_parse['meta_description']
						);
						$meta_title = str_replace(
							$array_string,
							$array_convert_string,
							$data['mc_meta_title']
						);
						$array_meta = array('desc' => $meta_description);
						$this->meta_description($post_id, $array_meta);
						$this->meta_title($post_id, $meta_title);
						$this->save_post_meta_locations($post_id, $mc_data_parse['post_title']);
						$this->save_post_meta_full_locations($post_id, $location);
						//meta
						$count_post_added++;

						do_action('loop_finish_parse_data', $post_id );
					}
				}
				wp_defer_term_counting( false );
				wp_defer_comment_counting( false );


			}
			return $count_post_added;
		}
		return false;
	}

	/**
	 * create page
	 * @param	$data	array()		pass data in array and parse it
	 * */
	public function create_page($data){
		//make sure data is array
		if(!get_page_by_title($data['post_title'])){
			$post = array(
			  'post_title'    => $data['post_title'],
			  'post_content'  => $data['post_content'],
			  'post_status'   => 'publish',
			  'post_type'   	=> 'page',
			  'post_parent'   => $data['post_parent']
			);
			// Insert the post into the database
			return wp_insert_post( $post );
		}
	}

	public function save_post_meta_locations($post_id, $location_name){
		update_post_meta($post_id, $this->post_meta_location_key, $location_name);
	}

	public function save_post_meta_full_locations($post_id, $location_name){
		update_post_meta($post_id, $this->post_meta_full_location_key, $location_name);
	}

	public function get_all_post_meta_locations(){
		global $wpdb;
		$query =
		 "
			SELECT meta_value, post_id
			FROM $wpdb->postmeta
			WHERE meta_key LIKE '%mc_post_meta_location%'
		";
		$suburb = $wpdb->get_results( $query );
		foreach ( $suburb as $val_suburb )
		{
			$array_suburb[] = array(
				'location' => $val_suburb->meta_value,
				'post_id' => $val_suburb->post_id,
			);
		}
		return $array_suburb;
	}

	public function get_post_meta_locations($limit = 5){
		 global $wpdb, $posts;
		 $array_suburb = array();
		 if( isset($posts[0]->post_title) ){
			 $current_location = $posts[0]->post_title;
		 }
		 $query =
		 "
			SELECT meta_value, post_id
			FROM $wpdb->postmeta
			WHERE meta_key LIKE '%mc_post_meta_location%'
			AND meta_key !='{$current_location}'
			ORDER BY RAND()
			LIMIT {$limit}
		";
		$suburb = $wpdb->get_results( $query );
		foreach ( $suburb as $val_suburb )
		{
			$array_suburb[] = array(
				'location' => $val_suburb->meta_value,
				'post_id' => $val_suburb->post_id,
			);
		}
		return $array_suburb;
	}

	public function meta_description($post_id, $array_desc){
		$ret = false;
		$desc = '';
		if(
			isset($array_desc['desc'])
			&& $array_desc['desc'] != ''
		){
			$desc = $array_desc['desc'];
		}
		//fallback incase yoast is not there
		update_post_meta($post_id, 'wp_metadesc', $desc);
		//for yoast
		update_post_meta($post_id, '_yoast_wpseo_metadesc', $desc);
	}

	public function meta_title($post_id, $location_name){
		update_post_meta($post_id, 'wp_meta_title', $location_name);
		update_post_meta($post_id, '_yoast_wpseo_title', $location_name);
	}

	public function __construct(){}
}
