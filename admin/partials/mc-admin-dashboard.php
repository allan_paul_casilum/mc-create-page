<div class="wrap about-wrap">
	<h1>Welcome to <?php echo mc_get_plugin_title();?> V<?php echo mc_get_plugin_version();?></h1>
	<hr>
	<?php if($this->get_input_error()){ ?>
		<div class="mc-error">
			<ul>
				<?php foreach($this->get_input_error() as $val) { ?>
						<li><p class="error"><?php echo $val;?></p></li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>
	<?php if($this->get_input_notice()){ ?>
		<div class="mc-sucess">
			<ul>
				<?php foreach($this->get_input_notice() as $val) { ?>
						<li><h3 class="sucess"><?php echo $val;?></p></h3>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>
	<?php require_once $form; ?>
</div>
