<div class="mc-form">
	<form name="mc_create_page" method="post" action="<?php echo $url_slug;?>">
		<input type="hidden" name="action" value="mc_parse_data">
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><label for="mc_data">Copy paste or Input data here to parse <span> *</span>: </label></th>
					<td>
						<textarea name="mc_data" rows="20" cols="100"></textarea>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="mc_parent_page">Choose parent page </label></th>
					<td>
						<?php if($pages) { ?>
							<select name="mc_parent_page">
								<option value="0">No Parent</option>
								<?php foreach($pages as $key_page => $val_page){ ?>
										<option value="<?php echo $val_page->ID;?>"><?php echo $val_page->post_title;?></option>
								<?php } ?>
							</select>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="mc_country">Google Map Country base, this is use for the google map embed <span> *</span>: </label></th>
					<td>
						<p>Put name of country, like: Australia, Philippines, China</p>
						<input type="text" name="mc_country" style="width:50%;">
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="<?php echo $editor_id;?>">Content</label></th>
					<td>
						<?php wp_editor( $content, $editor_id, $settings ); ?>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="mc_meta_title">Meta Title (use yoast plugin for better results)</label></th>
					<td>
						<p>The string %location-name% will replace the name of the location</p>
						<input type="text" name="mc_meta_title" value="<?php echo $meta_title;?>" style="width:100%;">
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="mc_meta_description">Meta Description (use yoast plugin for better results)</label>
					</th>
					<td>
						<p>The string %location-name% will replace the name of the location</p>
						<textarea name="mc_meta_description" rows="10" cols="50"><?php echo $meta_description_content;?></textarea>
					</td>
				</tr>
			</tbody>
		</table>

		<div class="template-filter-hook-additional-input">
			<?php
				if( has_filter('template_filter_hook_additional_input') ){
					$template = apply_filters('template_filter_hook_additional_input', $path);
					require_once $template;
				}
			?>
		</div>

		<p class="submit">
			<input type="submit" name="Submit" class="button-primary" value="Create Page now!" />
		</p>

	</form>
</div>
