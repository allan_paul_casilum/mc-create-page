<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public/partials
 */
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php if( is_array($locations) && count($locations) > 0 ){ ?>
	<ul id="mc-location">
		<?php for($i = 0; $i<=count($locations); $i++){ ?>
			<?php if( $locations[$i]['location'] != '' ){ ?>
				<li>
					<a href="<?php echo esc_url( get_permalink($locations[$i]['post_id']) ); ?>" class="mc-random-link">
						<span class="mc-random-link-title"><?php echo $locations[$i]['location'];?></span>
					</a>
				</li>
			<?php } ?>
		<?php } ?>
	</ul>
<?php } ?>
