<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           MC Create Page
 *
 * @wordpress-plugin
 * Plugin Name:       Automate Create Page
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Allan Paul Casilum
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-name
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
require_once plugin_dir_path( __FILE__ ) . 'config.php';

spl_autoload_register( 'mc_load_include_class' );
function mc_load_include_class( $class_name ) {
	if ( false !== strpos( $class_name, 'MC_' ) ) {
		$include_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
		$admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR;
		$class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
		if( file_exists($include_classes_dir . $class_file) ){
			require_once $include_classes_dir . $class_file;
		}
		if( file_exists($admin_classes_dir . $class_file) ){
			require_once $admin_classes_dir . $class_file;
		}
	}
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_mc_create_page() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mc-create-page-activator.php';
	WP_MC_Create_Page_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_mc_create_page() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mc-create-page-deactivator.php';
	WP_MC_Create_Page_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_mc_create_page' );
register_deactivation_hook( __FILE__, 'deactivate_mc_create_page' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mc-create-page.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/mc-function.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new WP_MC_Create_Page();
	$plugin->run();
	//include shortcode
	add_action( 'plugins_loaded', array( 'MC_Shortcode_GoogleMap', 'get_instance' ) );
	add_action( 'plugins_loaded', array( 'MC_Shortcode_RandomSuburb', 'get_instance' ) );
	add_action( 'plugins_loaded', array( 'MC_Shortcode_ListSuburb', 'get_instance' ) );
	add_action( 'plugins_loaded', array( 'MC_Shortcode_LocationName', 'get_instance' ) );
	add_action( 'plugins_loaded', array( 'MC_Shortcode_SuburbLink', 'get_instance' ) );
	add_action( 'plugins_loaded', array( 'MC_Shortcode_SuburbLocationName', 'get_instance' ) );
}
run_plugin_name();

if( is_admin() ){
	//load admin dependencies
	require plugin_dir_path( __FILE__ ) . 'admin/admin.php';
}
