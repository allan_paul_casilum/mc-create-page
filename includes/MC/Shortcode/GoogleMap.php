<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Shortcode_GoogleMap{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function init($atts){
		global $mc_config, $posts;
		$page = new MC_Page;
		$location = get_post_meta(get_the_ID(), $page->post_meta_full_location_key, true);
		$a = shortcode_atts( array(
			'location' => $location,
			'width' => '450',
			'height' => '600',
		), $atts );
		if( mc_google_map_embed_api() !='' ){
			$frame = '<iframe
			  width="'.$a['width'].'"
			  height="'.$a['height'].'"
			  frameborder="0" style="border:0"
			  src="https://www.google.com/maps/embed/v1/place?key='.mc_google_map_embed_api().'
				&q='.$a['location'].'" allowfullscreen>
			</iframe>';
			return $frame;
		}else{
			return 'error in google map public api, setup a credential key first';
		}
	}

	public function __construct(){
		add_shortcode( 'mc_google_map_embed', array($this, 'init') );
	}
}
