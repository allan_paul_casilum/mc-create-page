<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public/partials
 */
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php if( is_array($list_suburb) && count($list_suburb) > 0 ){ ?>
		<ul id="list-suburb" class="triple">
			<?php for($i = 0; $i<=count($list_suburb); $i++){//loop ?>
					<li>
						<a href="<?php echo esc_url( get_permalink($list_suburb[$i]['post_id']) ); ?>" class="mc-list-suburb-link">
							<span class="mc-list-suburb-link-title"><?php echo $list_suburb[$i]['location'];?></span>
						</a>
					</li>
			<?php }//loop end ?>
		</ul>
<?php }//if ?>
