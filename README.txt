=== Plugin Name ===
Contributors: allancasilum
Donate link: http://example.com/
Tags: automated, page, creation
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The plugin use to automate or create bulk pages

== Description ==

The plugin use to automate or create bulk pages

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload mc-create-page zip, unzip it to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =


== Screenshots ==


== Changelog ==

= 1.1 =
* Added: Hook action on parse data loop,
name of hook: 'loop_finish_parse_data' with $post_id as the ID of the post
* Added: Hook filter on plugin admin page to add more inputs,
name of filter 'template_filter_hook_additional_input' what it does is to require a template

= 1.0 =
* first version

== Upgrade Notice ==


== Arbitrary section ==


== A brief Markdown Example ==


