<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Init{
	public $array_error = array();
	public $array_notice = array();

	public function set_input_error($array_error){
		$this->array_error = $array_error;
	}

	public function get_input_error(){
		return $this->array_error;
	}

	public function set_input_notice($array_notice){
		$this->array_notice = $array_notice;
	}

	public function get_input_notice(){
		return $this->array_notice;
	}

	public function get_action(){
		if( isset($_GET['action']) ){
			$action = $_GET['action'];
		}elseif( isset($_POST['action']) ){
			$action = $_POST['action'];
		}
		return sanitize_text_field($action);
	}

	public function view_partials(){
		return mc_admin_partials();
	}

}
