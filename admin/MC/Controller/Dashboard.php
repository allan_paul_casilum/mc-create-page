<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class MC_Controller_Dashboard extends MC_Init{
	protected static $instance = null;
	public $model_dashboard;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'mc_parse_data':
				$error 		= array();
				$has_error 	= false;
				if( sanitize_text_field($_POST['mc_data']) == '' ){
					$has_error 	= true;
					$error[] = 'Please add data	to parse';
				}
				if( sanitize_text_field($_POST['mc_country']) == '' ){
					$has_error 	= true;
					$error[] = 'Please add country';
				}
				if( $has_error ){
					$this->set_input_error($error);
				}else{
					$page_added = $this->model_dashboard->parse_data($_POST);
					$notice[] = 'Successfully Added '. $page_added .' Pages ';
					$this->set_input_notice($notice);
				}
				$this->index();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function form(){
		$url_slug = $this->model_dashboard->menu_slug();
		return $this->view_partials() . 'mc-admin-form.php';
	}

	public function index(){
		$form = $this->form();
		$pages = get_pages();

		$content = '';
		$content = '<p>Add Content for [mc_suburb_name]</p>';
		$content .= '<p>[mc_google_map_embed]</p>';
		$content .= '<h3>Other Sydney Suburbs</h3>';
		$content .= '[mc_random_suburb]<br>';
		$content .= '<h3>Frequent Searches Leading To This Page</h3>';
		$content .= '[mc_suburb_link html_title="air conditioning"], [mc_suburb_link html_title="ducted air conditioning"] [mc_suburb_link html_title="split system air conditioning"],[mc_suburb_link html_title="air conditioning installation"]  ';

		$editor_id = 'mc_canned_content';
		$meta_description_content = 'add meta description here for %location-name%';
		$meta_title = 'Air Conditioning %location-name% - Air Conditioner Installation &amp; Service %location-name%';
		$settings = array( 'media_buttons' => false, 'editor_height' => 250, 'teeny' => true );
		require_once $this->view_partials() . 'mc-admin-dashboard.php';
	}

	public function __construct(){
		$this->model_dashboard = new MC_Dashboard;
	}
}
