<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
// hook admin menu
add_action( 'admin_menu', 'register_mc_menu_page' );
function register_mc_menu_page(){
	add_menu_page(
		'MC Create Page',
		'MC Create Page',
		'manage_options',
		mc_get_plugin_name(),
		array(MC_Controller_Dashboard::get_instance(), 'controller'),
		'',
		22
	);
}
